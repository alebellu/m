(ns org.ssoup.m
  (:refer-clojure :exclude [eval instance? get get-in])
  (:require [clojure.core.async :refer [chan pub sub <! put!]]
    #?(:clj
            [org.ssoup.async :refer [go exception? throw-err throw-err-with-data
                                     when-err-throw
                                     <? <?all channel?]]))
  #?(:cljs (:require-macros [org.ssoup.async-cljs
                             :refer [go exception? throw-err throw-err-with-data
                                     when-err-throw
                                     <? <?all channel?]])))

;---------------
; Constants.
;---------------

(defonce m-types {:m/Resource                nil
                  :m/Literal                 :m/Resource
                  :m/Object                  :m/Resource
                  :m/Type                    :m/Object
                  :m/LiteralType             :m/Type
                  :m/Class                   :m/Type
                  :m/Enum                    :m/Type
                  :m/EnumValue               :m/Object
                  :m/StaticEnum              :m/Enum
                  :m/PropertyFileEnum        :m/Enum
                  :m/DynamicEnum             :m/Enum
                  :m/I18nString              :m/Object
                  :m/Function                :m/Object
                  :m/FunctionArgument        :m/Object
                  :m/FunctionCall            :m/Object
                  :m/FunctionArgumentValue   :m/Object
                  :m/Value                   :m/Object
                  :m/InlineValue             :m/Value
                  :m/ParametrizedObjectValue :m/Value
                  :m/FunctionCallReturnValue :m/Object})

(defonce m-h (let [h (make-hierarchy)]
               (reduce (fn [h [t p]]
                         (if p (derive h t p) h)) h m-types)))

(defonce default-ctx {:h m-h})

;---------------
; M functions.
; M functions must accept as input arguments:
; - in : input parameters
;        - m/nodes A seq of nodes ids to consider in order to execute f. Starting from the first node,
;                  f will be executed on each node until the stop condition is satisfied.
;        - m/stop-policy One of:
;                        - :m/first-not-null Will stop at the first node where a not-null result is returned from calling f.
; - context : the current context
; - callback : the callback to be invoked when the function terminates, either successfully or with an error
; Callbacks must accepts the following arguments:
; - err : the error details, if an error occurred
; - data : result data
;---------------

; The id of the current node
; The current node is only a dispatch node, it does not implement any actual functionality.
; The implementation of the M api will be provided by the connected nodes
(defonce dispatch-node-id (atom nil))

; All nodes running in current operating system process
(defonce nodes (atom {:by-id   {}
                      :by-cost []}))

; The state of all the nodes,mapped by node-id.
(defonce state (atom {}))

; The functions specifications, mapped by fn spec id
(defonce fn-specs (atom {}))

; The functions implementations, mapped by impl id and fn spec id.
; by-id : entry values are pairs [node-id fn-impl]
; by-spec-id: entry values are arrays of pairs [node-id fn-impl]
(defonce fns (atom {:by-id      {}
                    :by-spec-id {}}))

; A black list of functions that should be excluded from statistics
(defonce stats-black-list (atom #{:m/send-function-start-statistics
                                  :m/send-function-end-statistics
                                  :m/report-error}))

(declare get-current-node)
(declare get-node-definition)
(declare get-connector)
(declare send-function-start-statistics)
(declare send-function-end-statistics)
(declare report-error)

(defn get-nodes
  "Returns all the registered nodes, ordered by cost"
  []
  (clojure.core/get @nodes :by-cost))

(defn get-node
  "Return the definition of a node given the id"
  [node-id]
  (clojure.core/get-in @nodes [:by-id node-id]))

(defn- specs-equals?
  "Checks whether specs a and b are the same"
  [a b]
  (= a b))

(defn- do-add-fn-spec
  "Adds a function specification to the registry provided
  that a spec with the same id does not already exist.
  If instead a spec with the same id already exists, then
  the 2 specs must match or an error is thrown."
  [fn-spec fn-specs]
  (if-not (contains? fn-specs (:m/id fn-spec))
    (assoc fn-specs (:m/id fn-spec) fn-spec)
    (if (specs-equals? fn-spec (clojure.core/get fn-specs (:m/id fn-spec)))
      fn-specs
      (throw-err ::spec-id-already-exists
                 "Spec with id " (:m/id fn-spec)
                 " already exist with a different definition"))))

(defn- add-fn-specs
  "Adds the specs from an api to the given specs register"
  [api fn-specs]
  (let [fn-specs                                            ; from plain :fn-specs specified in the api
        (reduce
          (fn [fn-specs fn-spec]
            (do-add-fn-spec fn-spec fn-specs))
          fn-specs
          (:m/fn-specs api))
        fn-specs                                            ; derived from fn implementations in the api
        (reduce
          (fn [fn-specs fn-def]
            (when-let [fn-spec (:m/fn-spec fn-def)]
              (let [fn-spec (if (:m/id fn-spec)
                              fn-spec
                              ; use fn-impl-id as fn-spec-id too
                              (assoc fn-spec :m/id (:m/id fn-def)))]
                (do-add-fn-spec fn-spec fn-specs))))
          fn-specs
          (:m/fns api))]
    fn-specs))

(defn- fn-implements-spec?
  "Checks whether a function implements a specification"
  [fn-def spec-id]
  (if-not (contains? @fn-specs spec-id)
    (throw-err ::spec-id-not-found "Spec with id " spec-id " does not exist in "
               @fn-specs ".")
    true                                                    ; TODO: add some checks?
    ))

(defn- do-add-fn
  "Adds a function implementation to the registry provided
  that a spec or a spec id for the function is provided, and that
  the implementation satisfies the spec constraints"
  [fn-def node fns]
  (if-not (contains? fns (:m/id fn-def))
    (let [node-id (:m/id node)
          spec-id (or (:m/spec-id fn-def) (:m/id (:m/spec fn-def)))]
      (if (fn-implements-spec? fn-def spec-id)
        (cond-> fns
                (:m/id fn-def) (assoc-in [:by-id (:m/id fn-def)] [node-id fn-def])
                true (update-in [:by-spec-id spec-id] concat [node-id fn-def]))
        (throw-err ::fn-spec-not-satisfied
                   "Function with id " (:m/id fn-def) " does not satisfy spec " spec-id)))
    (throw-err ::fn-id-already-registered
               "Function with id " (:m/id fn-def) " already registered")))

(defn- add-fns
  "Adds the implementations from an api to the given implementation register"
  [node fns]
  (let [fns
        (reduce
          (fn [fns [spec-id fn-def]]
            (if (fn? fn-def)
              (do-add-fn                                    ; anonymous function implementation
                #:m{:spec-id spec-id
                    :impl    fn-def}
                node fns)
              (if (map? fn-def)
                (do-add-fn
                  (assoc fn-def :spec-id spec-id)
                  node fns)
                (throw-err-with-data ::unrecognized-fn-def {:fn-def fn-def}
                                     "Unrecognized function definition"))))
          fns
          (-> node :m/api :m/fns-by-spec))
        fns
        (reduce
          (fn [fns fn-def]
            (do-add-fn fn-def node fns))
          fns
          (-> node :m/api :m/fns))]
    fns))

(defn get-state [node-id]
  (clojure.core/get @state node-id))

(defn swap-state!
  ([node-id f] (swap! state (fn [state]
                              (assoc state node-id (f (clojure.core/get state node-id))))))
  ([node-id f x] (swap! state (fn [state]
                                (assoc state node-id (f (clojure.core/get state node-id) x)))))
  ([node-id f x y] (swap! state (fn [state]
                                  (assoc state node-id (f (clojure.core/get state node-id) x y)))))
  ([node-id f x y & args] (swap! state (fn [state]
                                         (assoc state node-id (apply f (clojure.core/get state node-id) x y args))))))

(defn reset-state!
  [node-id new-state]
  (swap! state assoc node-id new-state))

(defn get-dispatch-node
  "Return the dispatch node definition"
  []
  (get-node :m/dispatch-node))

(defn satisfies-filter?
  "Checks whether a node satisfies a given filter. The filter can specify:
   - :m/node-id: the id of the node
   - :m/labels: the labels the node must have"
  [node {:m/keys [node-id node-labels] :as node-filter}]
  (or (= node-id (:m/id node))
      (every? (:m/labels node) node-labels)))

(defn filter-nodes
  "Filter nodes with a given filter."
  [nodes {:m/keys [node-id node-labels] :as nodes-filter}]
  (if node-id
    (clojure.core/get-in nodes [:by-id node-id])
    (filter
      (fn [node]
        (satisfies-filter? node nodes-filter))
      nodes)))

(defn multi-filter-nodes
  "Apply a sequence of filters one by one to a sequence of nodes and
   returns a single sequence of filtered nodes."
  [nodes filters]
  (->> filters
       ; apply all filters one by one to the original seq of nodes
       (map #(filter-nodes nodes %))
       ; merge the filtered nodes in a single array
       (reduce (fn [a b] (apply conj a b)) [])
       ; remove duplicate nodes
       distinct))

(defn- determine-target-nodes [fn-keyword context {:m/keys [target-node-filters target-node-id target-node-labels] :as in}]
  (let [nodes @nodes
        calling-node (or (:m/calling-node context) (get-dispatch-node))
        ; target-node-id takes precedence on target-node-labels which in turn takes precedence on target-node-filters
        target-node-filters (if target-node-id
                              [#:m{:node-id target-node-id}]
                              (if target-node-labels
                                [#:m{:node-labels target-node-labels}]
                                target-node-filters))
        target-node-filters (or target-node-filters
                                (clojure.core/get-in calling-node [:m/default-target-node-filters fn-keyword]))
        target-nodes (multi-filter-nodes (:by-cost nodes) target-node-filters)]
    target-nodes))

(defn- resolve-args
  "Resolve the arguments, waiting for async channels if needed."
  [args]
  (go (loop [args args
             resolved-args {}]
        (if (empty? args)
          resolved-args
          (let [[arg-name arg-value] (first args)]
            (recur (dissoc args arg-name)
                   (assoc resolved-args
                     arg-name
                     (if (channel? arg-value) (<? arg-value) arg-value))))))))

(defn- resolve-args-list
  "Resolve the arguments, waiting for async channels if needed."
  [args]
  (go (loop [args args
             resolved-args []]
        (if (empty? args)
          resolved-args
          (let [arg (first args)]
            (recur (next args)
                   (conj resolved-args (<? arg))))))))

(defn- find-fn-impls-by-spec [fn-spec-id]
  (clojure.core/get-in @fns [:by-spec-id fn-spec-id]))

(defn- find-fn-impl-best-match
  [context {:m/keys [fn-spec-id target-node-filters target-node-labels target-node-id] :as in}]
  (let [fn-defs (find-fn-impls-by-spec fn-spec-id)
        accept-fn-def?
        (fn [[node-id fn-def]]
          (let [target-node (get-node node-id)]
            (satisfies-filter?
              target-node
              #:m{:node-filters target-node-filters
                  :node-id      target-node-id
                  :node-labels  target-node-labels})))]
    (first (filter accept-fn-def? fn-defs))))

(defonce fn-impl-resolver (atom nil))

(defn set-fn-impl-resolver! [r]
  (reset! fn-impl-resolver r))

(defn- find-fn-impl
  [context {:m/keys [fn-id fn-spec-id] :as in}]
  (let [[node-id fn-impl] (when fn-id (clojure.core/get-in @fns [:by-id fn-id]))
        fn-spec-id (or fn-spec-id (:m/spec-id fn-impl) (:m/id (:m/spec fn-impl)))
        _ (when-not fn-spec-id (throw-err ::fn-spec-id-missing
                                          "fn-spec-id not specified"))
        fn-spec (clojure.core/get @fn-specs fn-spec-id)
        _ (when-not fn-spec (throw-err ::fn-spec-id-not-found
                                       "Could not find fn spec " fn-spec-id))
        [node-id fn-impl]
        (if fn-impl
          [node-id fn-impl]
          (let [fn-impls (find-fn-impls-by-spec fn-spec-id)]
            (condp = (count fn-impls)
              0
              (throw-err ::fn-impl-not-found "Could not find fn implementation for spec"
                         fn-spec-id)
              1
              (first fn-impls)
              :else
              (if-let [r @fn-impl-resolver]
                (r context fn-spec fn-impls)
                (throw-err ::fn-connot-resolve-impl
                           "Multiple implementations found for " fn-spec-id
                           " and no resolver defined")))))
        _ (when-not fn-impl (throw-err ::fn-impl-not-found
                                       "Could not find fn impolementation"))]
    [node-id fn-impl]))

; Map of interceptors keyed by fn-spec-id and/or fn-id :
; Interceptors are then grouped by type: :before, :failure, :success, :after.
(defonce fn-interceptors
         (atom {:before {}
                :success {}
                :failure {}
                :after {}}))

(defn- find-interceptors
  [fn-spec-id fn-id]
  (merge-with concat
              (clojure.core/get-in @fn-interceptors [:by-spec-id fn-spec-id])
              (clojure.core/get-in @fn-interceptors [:by-fn-id fn-id])))

(def ^:dynamic context {})

(defn- call
  "The implementation to invoke will be determined in this order :
   1) specified directly via parameter fn-id
   2) if only fn-spec-id is specified, then:
     2a) provided as list in argument :m/target-nodes: each element should be a node
         definition object.
     2b) determined by applying filters :m/target-node-filters: a seq of objects with
         one of the following properties:
         - :m/node-id: the id of the target node;
         - :m/node-labels: a sets of labels: all the nodes with the given labels will be
           added to the list of possible targets, in ascending order of distance.
         When a single filter is needed, a shorter version of :m/target-node-filters can
         be provided via :m-target-node-id or :m/target-node-labels.
     2c) specified at the calling node definition level in the property
         :m/default-target-node-filters for the key fn-keyword."
  [{:m/keys [fn-id fn-spec-id args] :as in}]
  (go (let [[node-id fn-impl] (find-fn-impl context in)
            f (:m/clj-impl fn-impl)
            interceptors (find-interceptors fn-spec-id fn-id)]
        (let [args (<? (resolve-args-list (doall args)))
              call-stack (or (clojure.core/get context :m/call-stack)
                             [(get-dispatch-node)])
              calling-node (peek call-stack)
              target-node (get-node node-id)
              context (assoc context :m/node-id (:m/id target-node)
                                     :m/node target-node
                                     :m/state (get-state (:m/id target-node))
                                     :m/calling-node-id (:m/id calling-node)
                                     :m/calling-node calling-node
                                     :m/call-stack (conj call-stack target-node))]
          (binding [context context]
            (doall (map #(apply % context args) (:before interceptors)))
            (try
              (let [ret (<? (apply f args))]
                (doall (map #(apply % context args ret) (:success interceptors)))
                ret)
              (catch #?(:clj Throwable :cljs :default) e
                (println "An exception occurred: " e)
                (doall (map #(apply % context args e) (:failure interceptors))))
              (finally
                (doall (map #(apply % context args) (:after interceptors))))))))))

(defn register-node
  "Register the definition of a node, which can contain the following keys:
   - :m/id: the node id [Required]
   - :m/cost: the cost of using the services provided by the node. This is an heuristic value that is only used
        to compare the node with other nodes
   - :m/labels: a set of labels for the node
   - :m/config: a configuration map specifying the node configuration in a node dependant way
   - :m/initial-state: the initial state of the node
   - :m/initialized: a flag indicating whether the node has been already initialized
   - :m/init-fn: the function to be called to initialize the node
   - :m/api-version: the version of the M api implemented by this node
   - :m/api: the node provided implementation of the M api (or of a subset of it)
   - :m/default-target-node-filters: a mapping from api function keywords to a (possibly ordered) list of target node filters.
         This mapping is used when an M function is called from the node and no other target node preference was specified."
  [node]
  (assert (not (nil? (:m/id node))))
  (let [node (if (:m/type node) node (assoc node :m/type :m/Node))]
    (swap! nodes (fn [{:keys [by-id by-cost]}]
                   {:by-id   (assoc by-id (:m/id node) node)
                    :by-cost (sort-by :cost (conj by-cost node))}))
    (when-let [api (:m/api node)]
      (swap! fn-specs (partial add-fn-specs api))
      (swap! fns (partial add-fns api)))
    (when (:m/initial-state node)
      (swap! state assoc (:m/id node) (:m/initial-state node)))))

(defn init-and-register-node
  "Initialize and register a node"
  [node callback]
  (if (:m/initialized node)
    (do (register-node node)
        (callback nil node))
    (if-let [init-fn (:m/init-fn node)]
      (init-fn node (fn [err node]
                      (register-node (assoc node :initialized true))
                      (callback err node)))
      (do
        (register-node (assoc node :initialized true))
        (callback nil node)))))

(defn dispatch-on-node-type [context & _]
  (-> context :m/node :m/type))

(defmacro func
  [name & fdecl]
  (when-not (symbol? name)
    (throw-err ::first-arg-must-be-symbol "First argument to defn must be a symbol"))
  (let [m (if (string? (first fdecl))
            {:doc (first fdecl)}
            {})
        fdecl (if (string? (first fdecl))
                (next fdecl)
                fdecl)
        m (if (map? (first fdecl))
            (conj m (first fdecl))
            m)
        fdecl (if (map? (first fdecl))
                (next fdecl)
                fdecl)
        m (conj (if (meta name) (meta name) {}) m)
        sig fdecl
        _ (when (not (seq? sig))
            (throw-err ::invalid-signature "Invalid signature " sig " should be a list"))
        [params & body] sig
        _ (when (not (vector? params))
            (throw-err ::invalid-param-declaration
                       "Parameter declaration " params " should be a vector"))
        m (conj {:arglist params} m)
        implements (:m/implements m)]
    ; Register the spec
    (let [fn-spec-id (if implements
                       (if (keyword? implements)
                         implements
                         (when (fn? implements)
                           (:m/fn-spec-id (meta implements))))
                       (keyword (str (ns-name *ns*)) (str name)))
          fn-id (when body fn-spec-id)
          fn-spec (assoc m :m/id fn-spec-id)
          fn-meta #:m{:fn-id      fn-id
                      :fn-spec-id fn-spec-id}]
      (swap! fn-specs (partial do-add-fn-spec fn-spec))
      (when body
        (let [fn-impl-def (first body)
              fn-impl-args (first (filter vector? fn-impl-def)) ; first vector after fn
              impl (clojure.core/eval fn-impl-def)]
          (when-not (fn? impl)
            (throw-err-with-data ::invalid-fn-body {:body body}
                                 "Body must be a function"))
          ; TODO: do more in depth checks.
          (when-not (= fn-impl-args params)
            (throw-err ::fn-impl-does-not-conform-to-spec
                       "Fn impl does not conform to spec " fn-impl-args " : " params))
          (let [fn-def #:m{:id       fn-id
                           :spec-id  fn-spec-id
                           :clj-impl impl}]
            (swap! fns (partial do-add-fn fn-def {})))))
      ; Define fn-id as a function directly callable in clojure, for simplicity
      `(defn ~name [& ~'args]
         (call (assoc ~fn-meta :m/args ~'args))))))

(def fns-spec
  {:m/authenticate
   #:m{:label     "Authenticate user"
       :scope     :s/application
       :arguments #:m{:credentials #:m{:type :m/UserCredentials}}
       :impure    true}

   :m/signout
   #:m{:label     "Sign out the current user"
       :scope     :s/application
       :arguments #{}
       :impure    true}

   :m/ask-for-authentication
   #:m{:label     "Ask the user to authenticate"
       :scope     :s/application
       :arguments #{}
       :impure    true}

   :m/can?
   #:m{:label     "Checks whether the user has the specified permissions"
       :scope     :s/application
       :arguments #{}
       :impure    true}})

(def api-parts
  #:m{:authentication-api         #{:m/authenticate
                                    :m/signout}
      :authentication-request-api #{:m/ask-for-authentication}
      :authorization-api          #{:m/can?
                                    :m/ensure-auth}
      :util-api                   #{:m/get-local-str-id
                                    :m/get-scope-str-id
                                    :m/to-keyword
                                    :m/get-local-id
                                    :m/get-scope-id}
      :scope-api                  #{:m/get-default-scope
                                    :m/set-default-scope!
                                    :m/init-scope!
                                    :m/assert-scope
                                    :m/remove-scope!}
      :type-api                   #{:m/instance?
                                    :m/get-type-definition
                                    :m/assert-type}
      :store-api                  #{:m/store-object!
                                    :m/get-object
                                    :m/get-objects
                                    :m/subscribe}
      :object-api                 #{:m/resolve-ref
                                    :m/get
                                    :m/get-in}
      :fn-build-api               #{:m/build-function-call}
      :fn-run-api                 #{:m/call-function}
      :eval-api                   #{:m/eval}
      :stats-api                  #{:m/send-function-start-statistics
                                    :m/send-function-end-statistics}
      :io-api                     #{:m/report-error}})

(defn build-target-node-filters [api-parts]
  (reduce (fn [target-node-filters [api-part fn-keywords]]
            (reduce (fn [target-node-filters fn-keyword]
                      (let [current-labels (or (:m/node-labels (first (clojure.core/get target-node-filters fn-keyword)))
                                               #{})]
                        (assoc target-node-filters fn-keyword [#:m{:node-labels (conj current-labels api-part)}])))
                    target-node-filters fn-keywords))
          {} api-parts))

(def default-target-node-filters (build-target-node-filters api-parts))

(defn init
  "Initialize the current node and register a sequence of nodes, in sequence.
   A set of labels for the group of nodes controlled by this dispatch node
   can be specified: the labels will be added to each of the nodes in the group."
  [{:m/keys [nodes group-labels default-target-node-filters]} callback]
  (reset! org.ssoup.m/nodes {:by-id   {}
                             :by-cost []})
  (reset! org.ssoup.m/state {})
  (let [errors (atom {})
        done-nodes (atom #{})
        total-count (atom 0)]
    (doall (map (fn [node]
                  (init-and-register-node
                    node
                    (fn [err node]
                      (if err
                        (swap! errors assoc node err)
                        (swap! done-nodes conj (assoc node :m/initialized true)))
                      (when (= (swap! total-count inc) (count nodes))
                        (register-node #:m{:id          :m/dispatch-node ; id of dispatch node is always :m/dispatch-node
                                           :cost        0
                                           :labels      (apply conj #{:m/dispatcher} group-labels)
                                           :initialized true
                                           :default-target-node-filters
                                                        (merge org.ssoup.m/default-target-node-filters
                                                               default-target-node-filters)})
                        (callback nil #:m{:done-nodes @done-nodes
                                          :errors     @errors})))))
                nodes))))

;--------------
; M bus with pub/sub functionalities available to all nodes
;--------------

(def bus (chan))
(def bus-pub (pub bus :topic-id))

(defn publish
  "Publishes a message to a specific topic"
  [topic-id message]
  (put! bus {:topic-id topic-id
             :message  message}))

(defn subscribe
  "Subscribes to a specific topic"
  [topic-id callback]
  (let [sub-chan (chan)]
    (sub bus-pub topic-id sub-chan)
    (go (loop []
          (let [{:keys [message]} (<! sub-chan)]
            (callback nil message)
            (recur))))))

; Flow control
; Has to be implemented with macros, because functions will evaluate
; all paths (arguments) before execution, which is not what we want
; in case of control flow.

(defmacro if
  "If statement in m"
  [c p1 p2]
  `(go (if (<? ~c) (<? ~p1) (<? ~p2))))

;;(defmacro mlet*
;  [bindings & body]
;;  `(go-loop [binding (first bindings)]
;           ()))

;(defmacro mlet
;  [bindings & body]
; `(go (let []))
;  (assert-args
;    (vector? bindings) "a vector for its binding"
;   (even? (count bindings)) "an even number of forms in binding vector")
;  `(mlet* ~(destructure bindings) ~@body))



;;(mlet [a (can? {:m/i1 1 :m/i2 2})
;;       b (add a 1)]
;      )

; Default (predefined) m functions
; From now on

(func signout
  "Signout the current user"
  #:m{}
  [])

(func can?
  "Checks whether or not an user has the rights to
   execute a given function with some given arguments"
  ^:impure ^:application-scoped
  [^:m/string a ^:m/integer b c]
  )

(func can?-impl
  "An implementation of can?"
  ^#:m{:implements #'can?}
  [^:m/string a ^:m/integer b c]
  #_(...))

(func can-add?
  "Checks whether 2 items can be added"
  [a b]
  (fn [a b]
    (and (number? a) (number? b))))

(func add
  "Adding two integers"
  [^:m/integer a ^:m/integer b]
  (fn [a b]
    (org.ssoup.m/if (can-add? a b)
      (+ a b) 0)))

(go (println (<? (let [a 1 b 3]
                   (org.ssoup.m/if (can-add? a b) (add a b) 0)))))
