(ns org.ssoup.async
  (:require [clojure.core.async :as async]))

(defmacro exception? [v]
  (instance? Throwable v))

(defmacro throw-err [err-code & msg-parts]
  `(throw (ex-info (str ~@msg-parts) {:err-code ~err-code})))

(defmacro throw-err-with-data [err-code data & msg-parts]
  `(throw (ex-info (str ~@msg-parts) (merge {:err-code ~err-code} ~data))))

(defmacro when-err-throw [v]
  `(let [v# ~v]
     (if (exception? v#) (throw v#) v#)))

(defn channel?
  [x]
  (satisfies? clojure.core.async.impl.protocols/Channel x))

; See:
; http://swannodette.github.io/2013/08/31/asynchronous-error-handling

(defmacro go [& body]
  `(clojure.core.async/go
     (try ~@body
          (catch Throwable t#
            t#))))

(defmacro <? [& body]
  `(if (channel? ~@body)
     (when-err-throw
       (async/<! ~@body))
     ~@body))

(defmacro <?all
  "Runs async operations sequentially, blocks until all operations are finished or exception is raised.
   Returns a seq of results or throws the first error."
  [channels]
  `(loop [chans# ~channels
          ret# '()]
     (if (empty? chans#)
       (cond
         ()
       (into (if () (empty ~channels)
             ret#)) ; use the same type of collection
       (recur
         (rest chans#)
         (<? (first chans#) ret#))))))
