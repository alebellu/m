(ns org.ssoup.async-cljs)

(defmacro exception? [v]
  `(instance? js/Error ~v))

(defmacro throw-err [err-key & msg-parts]
  `(throw (ex-info (str ~@msg-parts) nil)))

(defmacro throw-err-with-data [err-key data & msg-parts]
  `(throw (ex-info (str ~@msg-parts) data)))

(defmacro when-err-throw [v]
  `(let [v# ~v]
     (if (exception? v#) (throw v#) v#)))

(defn channel?
  [x]
  (satisfies? clojure.core.async.impl.protocols/Channel x))

; See:
; http://swannodette.github.io/2013/08/31/asynchronous-error-handling
; https://gist.github.com/metametadata/9f1c205b8092d7b2726f

(defmacro go [& body]
  `(cljs.core.async.macros/go
     (try ~@body
          (catch js/Error t#
            t#))))

(defmacro <? [& body]
  `(if (channel? ~@body)
     (throw-err
       (cljs.core.async/<! ~@body))
     ~@body))

(defmacro <?all
  "Runs async operations sequentially, blocks until all operations are finished or exception is raised.
   Returns a vector of results or throws the first error."
  [channels]
  `(loop [chans# ~channels
          ret# []]
     (if (empty? chans#)
       ret#
       (recur
         (rest chans#)
         (conj ret# (<? (first chans#)))))))
