(ns org.ssoup.mold
  (:refer-clojure :exclude [eval instance? get get-in])
  (:require [clojure.core.async :refer [chan pub sub <! put!]]
    #?(:clj
            [org.ssoup.async :refer [go exception? throw-err throw-err-with-data
                                     when-err-throw
                                     <? <?all channel?]]))
  #?(:cljs (:require-macros [org.ssoup.async-cljs
                             :refer [go exception? throw-err throw-err-with-data
                                     when-err-throw
                                     <? <?all channel?]])))

;---------------
; Constants.
;---------------

(defonce m-types {:m/Resource                nil
                  :m/Literal                 :m/Resource
                  :m/Object                  :m/Resource
                  :m/Type                    :m/Object
                  :m/LiteralType             :m/Type
                  :m/Class                   :m/Type
                  :m/Enum                    :m/Type
                  :m/EnumValue               :m/Object
                  :m/StaticEnum              :m/Enum
                  :m/PropertyFileEnum        :m/Enum
                  :m/DynamicEnum             :m/Enum
                  :m/I18nString              :m/Object
                  :m/Function                :m/Object
                  :m/FunctionArgument        :m/Object
                  :m/FunctionCall            :m/Object
                  :m/FunctionArgumentValue   :m/Object
                  :m/Value                   :m/Object
                  :m/InlineValue             :m/Value
                  :m/ParametrizedObjectValue :m/Value
                  :m/FunctionCallReturnValue :m/Object})

(defonce m-h (let [h (make-hierarchy)]
               (reduce (fn [h [t p]]
                         (if p (derive h t p) h)) h m-types)))

(defonce default-ctx {:h m-h})

;---------------
; M functions.
; M functions must accept as input arguments:
; - in : input parameters
;        - m/nodes A seq of nodes ids to consider in order to execute f. Starting from the first node,
;                  f will be executed on each node until the stop condition is satisfied.
;        - m/stop-policy One of:
;                        - :m/first-not-null Will stop at the first node where a not-null result is returned from calling f.
; - context : the current context
; - callback : the callback to be invoked when the function terminates, either successfully or with an error
; Callbacks must accepts the following arguments:
; - err : the error details, if an error occurred
; - data : result data
;---------------

; The id of the current node
; The current node is only a dispatch node, it does not implement any actual functionality.
; The implementation of the M api will be provided by the connected nodes
(defonce dispatch-node-id (atom nil))

; All nodes running in current operating system process
(defonce nodes (atom {:by-id   {}
                      :by-cost []}))

; The state of all the nodes,mapped by node-id.
(defonce state (atom {}))

; The functions specifications, mapped by fn spec id
(defonce fn-specs (atom {}))

; The functions implementations, mapped by impl id and fn spec id.
; by-id : entry values are pairs [node-id fn-impl]
; by-spec-id: entry values are arrays of pairs [node-id fn-impl]
(defonce fns (atom {:by-id      {}
                    :by-spec-id {}}))

; A black list of functions that should be excluded from statistics
(defonce stats-black-list (atom #{:m/send-function-start-statistics
                                  :m/send-function-end-statistics
                                  :m/report-error}))

(declare get-current-node)
(declare get-node-definition)
(declare get-connector)
(declare send-function-start-statistics)
(declare send-function-end-statistics)
(declare report-error)

(defn get-nodes
  "Returns all the registered nodes, ordered by cost"
  []
  (clojure.core/get @nodes :by-cost))

(defn get-node
  "Return the definition of a node given the id"
  [node-id]
  (clojure.core/get-in @nodes [:by-id node-id]))

(defn- specs-equals?
  "Checks whether specs a and b are the same"
  [a b]
  (= a b))

(defn- do-add-fn-spec
  "Adds a function specification to the registry provided
  that a spec with the same id does not already exist.
  If instead a spec with the same id already exists, then
  the 2 specs must match or an error is thrown."
  [fn-spec fn-specs]
  (if-not (contains? fn-specs (:id fn-spec))
    (assoc fn-specs (:id fn-spec) fn-spec)
    (when-not (specs-equals? fn-spec (clojure.core/get fn-specs (:id fn-spec)))
      (throw-err ::spec-id-already-exists
                 "Spec with id " (:id fn-spec) " already exist with a different definition"))))

(defn- add-fn-specs
  "Adds the specs from an api to the given specs register"
  [api fn-specs]
  (let [fn-specs                                            ; from plain :fn-specs specified in the api
        (reduce
          (fn [fn-specs fn-spec]
            (do-add-fn-spec fn-spec fn-specs))
          fn-specs
          (:m/fn-specs api))
        fn-specs                                            ; derived from fn implementations in the api
        (reduce
          (fn [fn-specs fn-def]
            (when-let [fn-spec (:m/fn-spec fn-def)]
              (let [fn-spec (if (:m/id fn-spec)
                              fn-spec
                              ; use fn-impl-id as fn-spec-id too
                              (assoc fn-spec :m/id (:m/id fn-def)))]
                (do-add-fn-spec fn-spec fn-specs))))
          fn-specs
          (:m/fns api))]
    fn-specs))

(defn- fn-implements-spec?
  "Checks whether a function implements a specification"
  [fn-def spec-id]
  (if (contains? fn-specs spec-id)
    (throw-err ::spec-id-not-found "Spec with id " spec-id " does not exist")
    true                                                    ; TODO: add some checks?
    ))

(defn- do-add-fn
  "Adds a function implementation to the registry provided
  that a spec or a spec id for the function is provided, and that
  the implementation satisfies the spec constraints"
  [fn-def node fns]
  (if-not (contains? fns (:m/id fn-def))
    (let [node-id (:m/id node)
          spec-id (or (:m/spec-id fn-def) (:m/id (:m/spec fn-def)))]
      (if (fn-implements-spec? fn-def spec-id)
        (cond-> fns
                (:m/id fn-def) (assoc-in [:by-id (:m/id fn-def)] [node-id fn-def])
                true (update-in [:by-spec-id spec-id] concat [node-id fn-def]))
        (throw-err ::fn-spec-not-satisfied
                   "Function with id " (:m/id fn-def) " does not satisfy spec " spec-id)))
    (throw-err ::fn-id-already-registered
               "Function with id " (:m/id fn-def) " already registered"))) Ø

(defn- add-fns
  "Adds the implementations from an api to the given implementation register"
  [node fns]
  (let [fns
        (reduce
          (fn [fns [spec-id fn-def]]
            (if (fn? fn-def)
              (do-add-fn                                    ; anonymous function implementation
                #:m{:spec-id spec-id
                    :impl    fn-def}
                node fns)
              (if (map? fn-def)
                (do-add-fn
                  (assoc fn-def :spec-id spec-id)
                  node fns)
                (throw-err-with-data ::unrecognized-fn-def {:fn-def fn-def}
                                     "Unrecognized function definition"))))
          fns
          (-> node :m/api :m/fns-by-spec))
        fns
        (reduce
          (fn [fns fn-def]
            (do-add-fn fn-def node fns))
          fns
          (-> node :m/api :m/fns))]
    fns))

(defn get-state [node-id]
  (clojure.core/get @state node-id))

(defn swap-state!
  ([node-id f] (swap! state (fn [state]
                              (assoc state node-id (f (clojure.core/get state node-id))))))
  ([node-id f x] (swap! state (fn [state]
                                (assoc state node-id (f (clojure.core/get state node-id) x)))))
  ([node-id f x y] (swap! state (fn [state]
                                  (assoc state node-id (f (clojure.core/get state node-id) x y)))))
  ([node-id f x y & args] (swap! state (fn [state]
                                         (assoc state node-id (apply f (clojure.core/get state node-id) x y args))))))

(defn reset-state!
  [node-id new-state]
  (swap! state assoc node-id new-state))

(defn get-dispatch-node
  "Return the dispatch node definition"
  []
  (get-node :m/dispatch-node))

(defn satisfies-filter?
  "Checks whether a node satisfies a given filter. The filter can specify:
   - :m/node-id: the id of the node
   - :m/labels: the labels the node must have"
  [node {:m/keys [node-id node-labels] :as node-filter}]
  (or (= node-id (:m/id node))
      (every? (:m/labels node) node-labels)))

(defn filter-nodes
  "Filter nodes with a given filter."
  [nodes {:m/keys [node-id node-labels] :as nodes-filter}]
  (if node-id
    (clojure.core/get-in nodes [:by-id node-id])
    (filter
      (fn [node]
        (satisfies-filter? node nodes-filter))
      nodes)))

(defn multi-filter-nodes
  "Apply a sequence of filters one by one to a sequence of nodes and
   returns a single sequence of filtered nodes."
  [nodes filters]
  (->> filters
       ; apply all filters one by one to the original seq of nodes
       (map #(filter-nodes nodes %))
       ; merge the filtered nodes in a single array
       (reduce (fn [a b] (apply conj a b)) [])
       ; remove duplicate nodes
       distinct))

(defn- determine-target-nodes [fn-keyword context {:m/keys [target-node-filters target-node-id target-node-labels] :as in}]
  (let [nodes @nodes
        calling-node (or (:m/calling-node context) (get-dispatch-node))
        ; target-node-id takes precedence on target-node-labels which in turn takes precedence on target-node-filters
        target-node-filters (if target-node-id
                              [#:m{:node-id target-node-id}]
                              (if target-node-labels
                                [#:m{:node-labels target-node-labels}]
                                target-node-filters))
        target-node-filters (or target-node-filters
                                (clojure.core/get-in calling-node [:m/default-target-node-filters fn-keyword]))
        target-nodes (multi-filter-nodes (:by-cost nodes) target-node-filters)]
    target-nodes))

(defn- resolve-args
  "Resolve the arguments, waiting for async channels if needed."
  [args]
  (go (loop [args args
             resolved-args {}]
        (if (empty? args)
          resolved-args
          (let [[arg-name arg-value] (first args)]
            (recur (dissoc args arg-name)
                   (assoc resolved-args
                     arg-name
                     (if (channel? arg-value) (<? arg-value) arg-value))))))))

(defn- find-fn-def-best-match
  [context {:m/keys [fn-spec-id target-node-filters target-node-labels target-node-id] :as in}]
  (let [fn-defs (clojure.core/get-in @fns [:by-spec-id fn-spec-id])
        accept-fn-def?
        (fn [[node-id fn-def]]
          (let [target-node (get-node node-id)]
            (satisfies-filter?
              target-node
              #:m{:node-filters target-node-filters
                  :node-id      target-node-id
                  :node-labels  target-node-labels})))]
    (first (filter accept-fn-def? fn-defs))))

(defn- find-fn-def
  [context {:m/keys [fn-id fn-spec-id] :as in}]
  (let [[node-id fn-def] (when fn-id (clojure.core/get-in @fns [:by-id fn-id]))
        fn-spec-id (or fn-spec-id (:m/spec-id fn-def) (:m/id (:m/spec fn-def)))
        _ (when-not fn-spec-id (throw-err ::fn-spec-id-missing
                                          "fn-spec-id not specified"))
        fn-spec (clojure.core/get @fn-specs fn-spec-id)
        _ (when-not fn-spec (throw-err ::fn-spec-id-not-found
                                       "Could not find fn spec " fn-spec-id))
        [node-id fn-def] (if fn-def [node-id fn-def]
                                    (find-fn-def-best-match context in))
        _ (when-not fn-def (throw-err ::fn-def-not-found
                                      "Could not find fn definition"))]
    [node-id fn-def]))

(declare ^:dynamic context {})

(defn- call
  "The implementation to invoke will be determined in this order :
   1) specified directly via parameter fn-id
   2) if only fn-spec-id is specified, then:
     2a) provided as list in argument :m/target-nodes: each element should be a node
         definition object.
     2b) determined by applying filters :m/target-node-filters: a seq of objects with
         one of the following properties:
         - :m/node-id: the id of the target node;
         - :m/node-labels: a sets of labels: all the nodes with the given labels will be
           added to the list of possible targets, in ascending order of distance.
         When a single filter is needed, a shorter version of :m/target-node-filters can
         be provided via :m-target-node-id or :m/target-node-labels.
     2c) specified at the calling node definition level in the property
         :m/default-target-node-filters for the key fn-keyword."
  [{:m/keys [fn-id fn-spec-id args] :as in}]
  (go (let [[node-id fn-def] (find-fn-def context in)
            f (:m/fn-impl fn-def)]
        (if-not f
          (throw-err-with-data ::fn-impl-not-found {:fn-def fn-def}
                               "Could not find fn implementation")
          (let [args (<? (resolve-args args))
                call-stack (or (clojure.core/get context :m/call-stack)
                               [(get-dispatch-node)])
                calling-node (peek call-stack)
                target-node (get-node node-id)
                context (assoc context :m/node-id (:m/id target-node)
                                       :m/node target-node
                                       :m/state (get-state (:m/id target-node))
                                       :m/calling-node-id (:m/id calling-node)
                                       :m/calling-node calling-node
                                       :m/call-stack (conj call-stack target-node))]
            (binding [context context]
              (when-not (contains? @stats-black-list fn-spec-id)
                (send-function-start-statistics nil (fn [err stats])))
              (try
                (f args)
                (finally
                  (when-not (contains? @stats-black-list fn-spec-id)
                    (send-function-end-statistics nil (fn [err stats])))))))))))

(defn- old-call-m-fn
  "fn-keyword is the fn spec id"
  [context {:m/keys [fn-id fn-spec-id async target-nodes stop-policy] :as in} callback]
  (let [[node-id fn-def] (when fn-id (clojure.core/get-in @fns [:by-id fn-id]))
        fn-spec-id (or fn-spec-id (:m/spec-id fn-def) (:m/id (:m/spec fn-def)))
        _ (when-not fn-spec-id (callback "fn spec id not specified"))
        fn-spec (clojure.core/get @fn-specs fn-spec-id)
        _ (when-not fn-spec (callback (str "Could not find fn spec " fn-spec-id)))
        [node-id fn-def] (if fn-def [node-id fn-def]
                                    (first (clojure.core/get-in @fns [:by-spec-id
                                                                      fn-spec-id])))
        _ (when-not fn-def (callback "Could not find fn definition"))]
    (let [target-node (get-node node-id)
          _ (satisfies-filter? target-node
                               #:m{:node-filters (clojure.core/get in :m/target-node-filters)
                                   :node-id      (clojure.core/get in :m/target-node-id)
                                   :node-labels  (clojure.core/get in :m/target-node-labels)})
          f (clojure.core/get-in target-node [:m/api fn-impl-id])
          call-stack (or (clojure.core/get context :m/call-stack) [(get-dispatch-node)])
          calling-node (peek call-stack)]
      (if f
        (do
          (when-not (contains? @stats-black-list fn-keyword)
            (send-function-start-statistics context nil (fn [err stats])))
          (let [context (assoc context :m/node-id (:m/id target-node)
                                       :m/node target-node
                                       :m/state (get-state (:m/id target-node))
                                       :m/calling-node-id (:m/id calling-node)
                                       :m/calling-node calling-node
                                       :m/call-stack (conj call-stack target-node))
                f-callback
                (fn f-callback
                  ([] (f-callback nil nil))
                  ([err] (f-callback err nil))
                  ([err data]
                   (when-not (contains? @stats-black-list fn-keyword)
                     (send-function-end-statistics context nil (fn [err stats])))
                   (if err
                     (callback err nil)
                     (if (and (nil? data) (= :m/FirstNotNull stop-policy))
                       (let [next-nodes (rest target-nodes)]
                         (if (not-empty next-nodes)
                           (call-m-fn fn-keyword context (assoc in :m/target-nodes next-nodes) callback)
                           (callback nil data)))
                       (callback err data)))))]
            (try
              (if async
                (f context in f-callback)
                (let [out (f context in)]
                  (f-callback nil out)
                  out))                                     ; also return the result directly
              (catch #?(:clj Throwable)
                     #?(:cljs js/Error) e
                                        (report-error context #:m{:error e})
                                        ; this can happen for sync function that send errors via exceptions.
                                        ; Normally async functions should send any error via the callback,
                                        ; but unexpected errors might end up here and we channel them
                                        ; to the callback anyway.
                                        (f-callback e)))))
        (let [next-nodes (rest target-nodes)]
          (if (not-empty next-nodes)
            (call-m-fn fn-keyword context (assoc in :m/target-nodes next-nodes) callback)
            (callback (ex-info (str "Could not find an implementation for M function " fn-keyword ". Calling node: " (:m/id calling-node))
                               #:m{:fn-keyword          fn-keyword
                                   :calling-node        (:m/id calling-node)
                                   :target-node-filters (:m/target-node-filters in)
                                   :target-node-labels  (:m/target-node-labels in)
                                   :target-node-id      (:m/target-node-id in)}) nil)))))))

(defn- call-m-fn-try-nodes
  "Deprecated: In the new version the implementing node is stored in the map fns and
   it is not needed to loop through the nodes to find the implementation.

   Target nodes for the function call will be determined in this order :
   1) provided as list in argument :m/target-nodes: each element should be a node definition object.
   2) determined by applying filters :m/target-node-filters: a seq of objects with one of the
      following properties:
      - :m/node-id: the id of the target node;
      - :m/node-labels: a sets of labels: all the nodes with the given labels will be added to the
         list of possible targets, in ascending order of distance.
      When a single filter is needed, a shorter version of :m/target-node-filters can be provided
      via :m-target-node-id or :m/target-node-labels.
   3) specified at the calling node definition level in the property :m/default-target-node-filters for the
      key fn-keyword."
  [fn-keyword context {:m/keys [async target-nodes stop-policy] :as in} callback]
  (let [target-nodes (or target-nodes
                         (determine-target-nodes fn-keyword context
                                                 (select-keys in [:m/target-node-filters
                                                                  :m/target-node-id
                                                                  :m/target-node-labels])))
        target-node (first target-nodes)
        f (clojure.core/get-in target-node [:m/api fn-keyword])
        call-stack (or (clojure.core/get context :m/call-stack) [(get-dispatch-node)])
        calling-node (peek call-stack)]
    (if f
      (do
        (when-not (contains? @stats-black-list fn-keyword)
          (send-function-start-statistics context nil (fn [err stats])))
        (let [context (assoc context :m/node-id (:m/id target-node)
                                     :m/node target-node
                                     :m/state (get-state (:m/id target-node))
                                     :m/calling-node-id (:m/id calling-node)
                                     :m/calling-node calling-node
                                     :m/call-stack (conj call-stack target-node))
              f-callback
              (fn f-callback
                ([] (f-callback nil nil))
                ([err] (f-callback err nil))
                ([err data]
                 (when-not (contains? @stats-black-list fn-keyword)
                   (send-function-end-statistics context nil (fn [err stats])))
                 (if err
                   (callback err nil)
                   (if (and (nil? data) (= :m/FirstNotNull stop-policy))
                     (let [next-nodes (rest target-nodes)]
                       (if (not-empty next-nodes)
                         (call-m-fn-try-nodes fn-keyword context (assoc in :m/target-nodes next-nodes) callback)
                         (callback nil data)))
                     (callback err data)))))]
          (try
            (if async
              (f context in f-callback)
              (let [out (f context in)]
                (f-callback nil out)
                out))                                       ; also return the result directly
            (catch #?(:clj Throwable)
                   #?(:cljs js/Error) e
                                      (report-error context #:m{:error e})
                                      ; this can happen for sync function that send errors via exceptions.
                                      ; Normally async functions should send any error via the callback,
                                      ; but unexpected errors might end up here and we channel them
                                      ; to the callback anyway.
                                      (f-callback e)))))
      (let [next-nodes (rest target-nodes)]
        (if (not-empty next-nodes)
          (call-m-fn-try-nodes fn-keyword context (assoc in :m/target-nodes next-nodes) callback)
          (callback (ex-info (str "Could not find an implementation for M function " fn-keyword ". Calling node: " (:m/id calling-node))
                             #:m{:fn-keyword          fn-keyword
                                 :calling-node        (:m/id calling-node)
                                 :target-node-filters (:m/target-node-filters in)
                                 :target-node-labels  (:m/target-node-labels in)
                                 :target-node-id      (:m/target-node-id in)}) nil))))))

#_(defn call-m-async-fn [fn-keyword context {:m/keys [target-nodes target-node-filters
                                                     stop-policy] :as in} callback]
  (call-m-fn fn-keyword context (assoc in :m/async true) callback))

#_(defn call-m-sync-fn [fn-keyword context {:m/keys [target-nodes target-node-filters
                                                    stop-policy] :as in}]
  (call-m-fn fn-keyword context (assoc in :m/async false)
             (fn [err _]                                    ; don't care about the output, it will also be returned by call-m-fn
               (when err
                 (throw err)))))

(defn register-node
  "Register the definition of a node, which can contain the following keys:
   - :m/id: the node id [Required]
   - :m/cost: the cost of using the services provided by the node. This is an heuristic value that is only used
        to compare the node with other nodes
   - :m/labels: a set of labels for the node
   - :m/config: a configuration map specifying the node configuration in a node dependant way
   - :m/initial-state: the initial state of the node
   - :m/initialized: a flag indicating whether the node has been already initialized
   - :m/init-fn: the function to be called to initialize the node
   - :m/api-version: the version of the M api implemented by this node
   - :m/api: the node provided implementation of the M api (or of a subset of it)
   - :m/default-target-node-filters: a mapping from api function keywords to a (possibly ordered) list of target node filters.
         This mapping is used when an M function is called from the node and no other target node preference was specified."
  [node]
  (assert (not (nil? (:m/id node))))
  (let [node (if (:m/type node) node (assoc node :m/type :m/Node))]
    (swap! nodes (fn [{:keys [by-id by-cost]}]
                   {:by-id   (assoc by-id (:m/id node) node)
                    :by-cost (sort-by :cost (conj by-cost node))}))
    (when-let [api (:m/api node)]
      (swap! fn-specs (partial add-fn-specs api))
      (swap! fns (partial add-fns api)))
    (when (:m/initial-state node)
      (swap! state assoc (:m/id node) (:m/initial-state node)))))

(defn init-and-register-node
  "Initialize and register a node"
  [node callback]
  (if (:m/initialized node)
    (do (register-node node)
        (callback nil node))
    (if-let [init-fn (:m/init-fn node)]
      (init-fn node (fn [err node]
                      (register-node (assoc node :initialized true))
                      (callback err node)))
      (do
        (register-node (assoc node :initialized true))
        (callback nil node)))))

(defn dispatch-on-node-type [context & _]
  (-> context :m/node :m/type))

;--------------
; M API BEGIN
;--------------

(def api-version 1)

(defn get-default-scope
  "Returns the default scope."
  [context in]
  (call-m-sync-fn :m/get-default-scope context in))

(defn set-default-scope!
  "Sets the default scope."
  [context {:m/keys [scope-id] :as in}]
  (call-m-sync-fn :m/set-default-scope! context in))

(defn init-scope! [context {:m/keys [scope-id parent-scope-id set-as-current error-if-existing] :as in}]
  "Defines a new scope with an optional parent scope.
    Will throw an error if the scope is already defined."
  (call-m-sync-fn :m/init-scope! context in))

(defn assert-scope
  "Asserts that the given scope exists."
  [context {:m/keys [scope-id] :as in}]
  (call-m-sync-fn :m/assert-scope context in))

(defn remove-scope!
  "Removes the scope specified.
    If it is the current scope,the current scope
    will be set to its parent scope."
  [context {:m/keys [scope-id] :as in}]
  (call-m-sync-fn :m/remove-scope! context in))

(defn get-local-str-id [context {:m/keys [object-str-id] :as in}]
  "Extract the local part from an object id in the form <local-id>@<scope-id>"
  (call-m-sync-fn :m/get-local-str-id context in))

(defn get-scope-str-id
  "Extract the scope part from an object id in the form <local-id>@<scope-id>"
  [context {:m/keys [object-str-id] :as in}]
  (call-m-sync-fn :m/get-scope-str-id context in))

(defn to-keyword
  "Converts an iri to a clojure keyword"
  [context {:m/keys [str] :as in}]
  (call-m-sync-fn :m/to-keyword context in))

(defn get-local-id
  "Extract the local id from an object id.
    When object-id is a keyword returns itself.
    Always returns keywordized local id."
  [context {:m/keys [object-id] :as in}]
  (call-m-sync-fn :m/get-local-id context in))

(defn get-scope-id
  "Extract the scope id from an object id.
    When object-id is a keyword returns nil.
    Always returns keywordized scope id."
  [context {:m/keys [object-id return-current-if-not-found] :as in}]
  (call-m-sync-fn :m/get-scope-id context in))

(defn instance?
  "Tests if obj is an instance of the class m-class in type hierarchy h."
  [context {:m/keys [class obj] :as in}]
  (call-m-sync-fn :m/instance? context in))

(defn store-object!
  "Stores an object in one of the nodes specified"
  [context {:m/keys [scope-id local-id object] :as in} callback]
  (call-m-async-fn :m/store-object! context in callback))

(defn get-object
  "Returns the object with the given id, if it is in the local store.
   If not, depending on the fetch policy for the type, the object can be searched for in connected nodes.
   A key+value pair can be specified instead of the id."
  [context {:m/keys [scope-id local-id object-id type-id key key-value] :as in} callback]
  (call-m-async-fn :m/get-object context in callback))

(defn get-objects
  "Returns the objects with the given id, if they are in the local store.
   If not, depending on the fetch policy for the type, the object can be searched for in connected nodes."
  [context {:m/keys [scope-id local-id objectids type-id] :as in} callback]
  (call-m-async-fn :m/get-objects context in callback))

(defn resolve-ref
  "Resolves references to objects.
     When the argument passed is not a keyword, returns the argument, unmodified.
     If the argument passed is a keyword, but no object is found with the given id, returns the argument, unmodified."
  [context {:m/keys [ref] :as in} callback]
  (call-m-async-fn :m/resolve-ref context in callback))

(defn get
  "Returns the value of property prop of object obj.
     If obj or the property value are references, they are resolved."
  [context {:m/keys [obj prop] :as in} callback]
  (call-m-async-fn :m/get context in callback))

(defn get-in
  "Returns the value of nested property addressed by props starting from object obj.
   If obj or encountered property values are references, they are resolved."
  [context {:m/keys [obj props] :as in} callback]
  (call-m-async-fn :m/get-in context in callback))

(defn get-type-definition
  "Returns the definition of the type with the given id"
  [context in]
  (call-m-sync-fn :m/get-type-definition context in))

(defn assert-type
  "Asserts that the type of the object is the expected one."
  [context {:m/keys [object type] :as in}]
  (call-m-sync-fn :m/assert-type context in))

(defn build-function-call
  "Creates an object that represents the invocation of a given function.
   Actual argument values can be supplied: if all required arguments are supplied,
   the function call is created with the supplied arguments.
   If, instead, some required arguments are not supplied, then the node will try
   to find a suitable value in a node dependant way.
   Will return the object representing the function call, or an error if some
   of the arguments could not be found."
  [context {:m/keys [f args default-args] :as in} callback]
  (call-m-async-fn :m/build-function-call context in callback))

(defn call-function
  "Calls an M function"
  [context {:m/keys [fn-keyword args] :as in} callback]
  (call-m-async-fn fn-keyword context (merge in args) callback))

(defn send-function-start-statistics
  "Sends the statistics associated with the execution of the function f, at the start of the execution"
  [context in callback]
  (call-m-async-fn :m/send-function-start-statistics context in callback))

(defn send-function-end-statistics
  "Sends the statistics associated with the execution of the function f, updated at the end of the execution"
  [context in callback]
  (call-m-async-fn :m/send-function-end-statistics context in callback))

(defn eval
  "Evaluates the form data structure (not text!):
     - Object refs will be expanded, if referenced object is found in
       the relevant scope
     - Parameter refs will be resolved
     - Function calls will be executed
     Accepts a callback that will receive the result of the evaluation."
  [context {:m/keys [form resolve-refs] :as in} callback]
  (call-m-async-fn :m/eval context in callback))

(defn report-error
  "Reports an error in a node dependant way"
  [context {:m/keys [error] :as in}]
  (call-m-sync-fn :m/report-error context in))

(defn authenticate
  "Authenticate an user to the system"
  [context {:m/keys [] :as in} callback]
  (call-m-async-fn :m/authenticate context in callback))

(defn ask-for-authentication
  "Requires that the user or the system is asked for authentication"
  [context {:m/keys [] :as in} callback]
  (call-m-async-fn :m/ask-for-authentication context in callback))

(defn signout
  "Signout the current user"
  [context in callback]
  (call-m-async-fn :m/signout context in callback))

(defmacro func
  ([fn-or-spec-id desc fn-spec]
   (func fn-or-spec-id desc fn-spec nil))
  ([fn-or-spec-id desc {:m/keys [implements scope args] :as fn-spec} fn-def]
    ; Register the spec
   (let [fn-id (when fn-def fn-or-spec-id)
         fn-spec-id (if implements
                      (if (keyword? implements)
                        implements
                        (when (fn? implements)
                          (:m/fn-spec-id (meta implements))))
                      ~(keyword *ns* fn-or-spec-id))
         fn-spec (merge #:m{:id        fn-spec-id
                            :desc      desc
                            :scope     scope
                            :arguments args}
                        fn-spec)
         fn-meta #:m{:fn-id      ~fn-id
                     :fn-spec-id ~fn-spec-id}]
     (swap! fn-specs (partial do-add-fn-spec fn-spec))
     ; Define fn-id as a fn directly callable in clojure, for simplicity
     (defn ~fn-id
        ~fn-meta
        [& args]
       (call (assoc ~fn-meta :m/args args))))))


(func report-error
  "Reports an error in a node dependant way"
  #:m{}
  [^:m/string error]
  (call-m-sync-fn :m/report-error context in))

(defn authenticate
  "Authenticate an user to the system"
  [context {:m/keys [] :as in} callback]
  (call-m-async-fn :m/authenticate context in callback))

(defn ask-for-authentication
  "Requires that the user or the system is asked for authentication"
  [context {:m/keys [] :as in} callback]
  (call-m-async-fn :m/ask-for-authentication context in callback))

(func signout
  "Signout the current user"
  #:m{})

(func can?
  "Checks whether or not an user has the rights to
   execute a given function with some given arguments"
  ^:impure ^:application-scoped
  [^:m/string a ^:m/integer b c]
  )

(func can?-impl
  "An implementation of can?"
  ^#:m{:implements #'can?}
  [^:m/string a ^:m/integer b c]
    #_(...))

(func can-add?
  "Checks whether 2 items can be added"
  [a b]
  true)

(func mif
  "If statement in m"
  [cond p1 p2]
  (if (<? cond) (<? p1) (<? p2)))

(func add
  "Adding two integers"
  [^:m/integer a ^:m/integer b]
  (mif (can-add? a b)
    (+ a b)
    0))

(can? 1 true 4)

(defmacro mlet*
  [bindings & body]
  `(go-loop [binding (first bindings)]
            ()))

(defmacro mlet
  [bindings & body]
  `(go (let []))



  (assert-args
    (vector? bindings) "a vector for its binding"
    (even? (count bindings)) "an even number of forms in binding vector")
  `(mlet* ~(destructure bindings) ~@body))



(mlet [a (can? {:m/i1 1 :m/i2 2})
       b (add a 1)]
      )

(defn ensure-auth
  "Checks whether or not an user is authenticated and has the given rights.
   If not it goes through the authentication and authorization process."
  [context {:m/keys [permisssions] :as in} callback]
  (call-m-async-fn :m/ensure-auth context in callback))

;--------------
; M API END
;--------------

(def fns-spec
  {:m/authenticate
   #:m{:label     "Authenticate user"
       :scope     :s/application
       :arguments #:m{:credentials #:m{:type :m/UserCredentials}}
       :impure    true}

   :m/signout
   #:m{:label     "Sign out the current user"
       :scope     :s/application
       :arguments #{}
       :impure    true}

   :m/ask-for-authentication
   #:m{:label     "Ask the user to authenticate"
       :scope     :s/application
       :arguments #{}
       :impure    true}

   :m/can?
   #:m{:label     "Checks whether the user has the specified permissions"
       :scope     :s/application
       :arguments #{}
       :impure    true}})

(def api-parts
  #:m{:authentication-api         #{:m/authenticate
                                    :m/signout}
      :authentication-request-api #{:m/ask-for-authentication}
      :authorization-api          #{:m/can?
                                    :m/ensure-auth}
      :util-api                   #{:m/get-local-str-id
                                    :m/get-scope-str-id
                                    :m/to-keyword
                                    :m/get-local-id
                                    :m/get-scope-id}
      :scope-api                  #{:m/get-default-scope
                                    :m/set-default-scope!
                                    :m/init-scope!
                                    :m/assert-scope
                                    :m/remove-scope!}
      :type-api                   #{:m/instance?
                                    :m/get-type-definition
                                    :m/assert-type}
      :store-api                  #{:m/store-object!
                                    :m/get-object
                                    :m/get-objects
                                    :m/subscribe}
      :object-api                 #{:m/resolve-ref
                                    :m/get
                                    :m/get-in}
      :fn-build-api               #{:m/build-function-call}
      :fn-run-api                 #{:m/call-function}
      :eval-api                   #{:m/eval}
      :stats-api                  #{:m/send-function-start-statistics
                                    :m/send-function-end-statistics}
      :io-api                     #{:m/report-error}})

(defn build-target-node-filters [api-parts]
  (reduce (fn [target-node-filters [api-part fn-keywords]]
            (reduce (fn [target-node-filters fn-keyword]
                      (let [current-labels (or (:m/node-labels (first (clojure.core/get target-node-filters fn-keyword)))
                                               #{})]
                        (assoc target-node-filters fn-keyword [#:m{:node-labels (conj current-labels api-part)}])))
                    target-node-filters fn-keywords))
          {} api-parts))

(def default-target-node-filters (build-target-node-filters api-parts))

(defn init
  "Initialize the current node and register a sequence of nodes, in sequence.
   A set of labels for the group of nodes controlled by this dispatch node
   can be specified: the labels will be added to each of the nodes in the group."
  [{:m/keys [nodes group-labels default-target-node-filters]} callback]
  (reset! org.ssoup.m/nodes {:by-id   {}
                             :by-cost []})
  (reset! org.ssoup.m/state {})
  (let [errors (atom {})
        done-nodes (atom #{})
        total-count (atom 0)]
    (doall (map (fn [node]
                  (init-and-register-node
                    node
                    (fn [err node]
                      (if err
                        (swap! errors assoc node err)
                        (swap! done-nodes conj (assoc node :m/initialized true)))
                      (when (= (swap! total-count inc) (count nodes))
                        (register-node #:m{:id          :m/dispatch-node ; id of dispatch node is always :m/dispatch-node
                                           :cost        0
                                           :labels      (apply conj #{:m/dispatcher} group-labels)
                                           :initialized true
                                           :default-target-node-filters
                                                        (merge org.ssoup.m/default-target-node-filters
                                                               default-target-node-filters)})
                        (callback nil #:m{:done-nodes @done-nodes
                                          :errors     @errors})))))
                nodes))))

;--------------
; M bus with pub/sub functionalities available to all nodes
;--------------

(def bus (chan))
(def bus-pub (pub bus :topic-id))

(defn publish
  "Publishes a message to a specific topic"
  [topic-id message]
  (put! bus {:topic-id topic-id
             :message  message}))

(defn subscribe
  "Subscribes to a specific topic"
  [topic-id callback]
  (let [sub-chan (chan)]
    (sub bus-pub topic-id sub-chan)
    (go-loop []
             (let [{:keys [message]} (<! sub-chan)]
               (callback nil message)
               (recur)))))
