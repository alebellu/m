(ns org.ssoup.json
  (:require [clojure.string]
    #?(:clj
            [clojure.data.json :as json])))

; to have faster parsing we could use Transit but would mean to add a dependency:
; in the future give the user the possibility to provide an external parser.
(defn parse-json [json-str]
  #?(:clj (json/read-str json-str))
  #?(:cljs (js->clj (.parse js/JSON json-str))))

(defn write-json [json-data]
  #?(:clj (json/write-str json-data :escape-slash false))
  #?(:cljs (.stringify js/JSON (clj->js json-data))))

; useful to represent strings starting with a :
(def string-prefix "#str#")

;-------------------------
; CLJ -> M JSON
;-------------------------

(declare write)

(defn- write-json-keyword [k]
  (if (namespace k)
    (str ":" (namespace k) ":" (name k))
    (str ":" (name k))))

(defn- write-json-string [s]
  (if (clojure.string/starts-with? s ":")
    (str string-prefix s)
    s))

(defn- write-json-map [map-data]
  (into {}
        (map (fn [[k v]]
               [(write k) (write v)])
             map-data)))

(defn- write-json-vec [data]
  (into [] (map write data)))

(defn- write-json-list [data]
  {"#list#" (write-json-vec data)})

(defn- write-json-set [data]
  {"#set#" (write-json-vec data)})

(defn write [data]
  (cond
    (keyword? data)
    (write-json-keyword data)

    (string? data)
    (write-json-string data)

    (map? data)
    (write-json-map data)

    (list? data)
    (write-json-list data)

    (vector? data)
    (write-json-vec data)

    (set? data)
    (write-json-set data)

    :else
    data))

(defn write-str [data]
  (write-json (write data)))

;-------------------------
; M JSON -> CLJ
;-------------------------

(declare parse)

(defn- parse-keyword [keyword-str]
  (let [keyword-str (subs keyword-str 1)
        tks (clojure.string/split keyword-str #":")]
    (if (> (count tks) 1)
      (keyword (first tks) (second tks))
      (keyword (first tks)))))

(defn- parse-json-string [str]
  (cond
    (clojure.string/starts-with? str ":")
    (parse-keyword str)

    (clojure.string/starts-with? str string-prefix)
    (subs str (count string-prefix))

    :else
    str))

(defn- parse-json-map [json-map]
  (let [clj-map (into {}
                      (map (fn [[k v]]
                             [(parse k) (parse v)])
                           json-map))]
    (cond
      (and (= 1 (count clj-map))
           (= "#set#" (key (first clj-map))))
      (into #{} (val (first clj-map)))

      (and (= 1 (count clj-map))
           (= "#list#" (key (first clj-map))))
      (into (list) (reverse (val (first clj-map))))

      (and (= 1 (count clj-map))
           (= "#vector#" (key (first clj-map))))            ; but this is also the default mapping of json arrays []
      (into [] (val (first clj-map)))

      :else
      clj-map)))

(defn- parse-json-vec [json-vec]
  (into [] (map parse json-vec)))

(defn parse [json]
  (cond
    (keyword? json)
    (parse-json-string (name json)) ; if keys were "keywordized" already, ignore it

    (string? json)
    (parse-json-string json)

    (map? json)
    (parse-json-map json)

    (vector? json)
    (parse-json-vec json)

    :else
    json))

(defn parse-str [json-str]
  (parse (parse-json json-str)))
