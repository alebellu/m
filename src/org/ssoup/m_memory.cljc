(ns org.ssoup.m-memory
  (:refer-clojure :exclude [eval instance? get get-in])
  (:require [clojure.string :as string]
            [org.ssoup.m :as m]
            [org.ssoup.async-walk :as w]))

;---------------
; M functions
;---------------

(defn send-function-start-statistics
  [context in callback])

(defn send-function-end-statistics
  [context in callback])

;---------------
; Scope api
;---------------

(defn ^{:id :mm/get-default-scope
        :spec {:id :m/get-default-scope
               :label "Returns the default scope."
               :description "Returns the default scope."
               :in {}
               :out {:m/scope :m/string}}}
  get-default-scope [{:m/keys [state] :as context} in]
  (let [cs (:m/default-scope state)]
    (assert (not (nil? cs)) "Default scope not set")
    cs))

(defn set-default-scope! [{:m/keys [node-id] :as context} {:m/keys [scope-id] :as in}]
  (assert (not (nil? scope-id)) "nil scope")
  (m/swap-state! node-id assoc :m/default-scope scope-id))

(defn init-scope! [{:m/keys [state node-id] :as context} {:m/keys [scope-id parent-scope-id set-as-default error-if-existing] :as in}]
  (let [scope-ids (:m/scope-ids state)]
    (when (and error-if-existing (clojure.core/get scope-ids scope-id))
      (throw (ex-info (str "Scope " scope-id " is already defined.")
                      {})))
    (when parent-scope-id
      (derive scope-id parent-scope-id))
    (m/swap-state! node-id update :m/scope-ids conj scope-id)
    (when set-as-default
      (m/set-default-scope! context #:m{:scope-id scope-id}))))

(defn assert-scope [{:m/keys [state node-id] :as context} {:m/keys [scope-id] :as in}]
  (let [scope-ids (:m/scope-ids state)]
    (assert (contains? scope-ids scope-id)
            (str "Undefined scope " scope-id))))

(defn remove-scope! [{:m/keys [state node-id] :as context} {:m/keys [scope-id] :as in}]
  (let [scope-ids (:m/scope-ids state)]
    (when (scope-ids scope-id)
      (when (= scope-id context (m/get-default-scope context nil))
        (m/set-default-scope! context #:m{:scope-id (first (parents scope-id))}))
      (m/swap-state! node-id update :m/scope-ids disj scope-id))))

;---------------
; Util api
;---------------

(defn get-local-str-id [context {:m/keys [object-str-id] :as in}]
  (let [idx (string/index-of object-str-id "@")]
    (if (pos-int? idx)
      (subs object-str-id 0 idx)
      object-str-id)))

(defn get-scope-str-id [context {:m/keys [object-str-id] :as in}]
  (let [idx (string/index-of object-str-id "@")]
    (when (pos-int? idx)
      (subs object-str-id (inc idx)))))

(defn to-keyword [context {:m/keys [str] :as in}]
  (when (clojure.string/starts-with? str ":")
    (keyword (subs str 1))))

(defn get-local-id [context {:m/keys [object-id] :as in}]
  (if (keyword? object-id)
    object-id
    (when (string? object-id)
      (m/to-keyword context #:m{:str (m/get-local-str-id context #:m{:object-str-id object-id})}))))

(defn get-scope-id [context {:m/keys [object-id return-current-if-not-found] :as in}]
  (let [s-id (when (string? object-id)
               (when-let [s-str-id (m/get-scope-str-id context #:m{:object-str-id object-id})]
                 (m/to-keyword context #:m{:str s-str-id})))]
    (or s-id (when return-current-if-not-found (m/get-default-scope context nil)))))

;---------------
; Type api
;---------------

(defn instance? [context {:m/keys [class obj] :as in}]
  (when obj
    (when-let [types (:m/type obj)]
      (let [h (:h context)]
        (if (vector? types)
          (some #(isa? h % class) types)
          (isa? h types class))))))

(defn get-type-definition [{:m/keys [state] :as context} {:m/keys [type-id] :as in}]
  (let [schema (:m/schema state)]
    (clojure.core/get-in schema [:types type-id])))

(defn assert-type [context {:m/keys [object type] :as in}]
  (when type
    (assert (= type (:m/type object)) (str "Object type " (:m/type object) " is not a " type))))

;---------------
; Store api
;---------------

(defn store-object! [{:m/keys [node-id node] :as context} {:m/keys [scope-id local-id object] :as in} callback]
  (let [config (:m/config node)
        scope-id (or scope-id (m/get-default-scope context nil))
        local-id (or local-id (:m/id object))]
    (when-let [object-id (:m/id object)]
      (assert (and (= scope-id (m/get-scope-id context #:m{:object-id object-id :return-current-if-not-found true}))
                   (= local-id (m/get-local-id context #:m{:object-id object-id})))))
    (m/assert-scope context #:m{:scope-id scope-id})
    ; Save the object in the local cache
    (m/swap-state! node-id assoc-in [:m/object-store scope-id local-id] object)
    (if (:m/persist-stored-objects config)
      ; Store the object in persistent stores.
      (m/store-object! context (assoc in :m/target-node-labels #{:m/persistent-store})
                       (fn [err data]
                         (if err
                           (callback err)
                           (callback))))
      (callback))))

(defn get-object [{:m/keys [state] :as context} {:m/keys [scope-id local-id object-id type-id] :as in} callback]
  (let [object-store (:m/object-store state)
        scope-id (or scope-id (m/get-scope-id context #:m{:object-id object-id :return-current-if-not-found true}))
        local-id (or local-id (m/get-local-id context #:m{:object-id object-id}))
        _ (m/assert-scope context #:m{:scope-id scope-id})
        object (clojure.core/get-in object-store [scope-id local-id])]
    (if object
      (callback nil object)
      (if type-id
        (let [td (m/get-type-definition context #:m{:type-id type-id})]
          (if (= :m/fetchAndCache (:m/fetchPolicy td))      ; when no local object is found, search persistent stores.
            (m/get-object context (merge in #:m{:target-node-labels #{:m/persistent-store}
                                                :stop-policy        :m/FirstNotNull})
                          (fn [err object]                  ; data is returned from one of the connected nodes
                            (if err
                              (callback err)
                              (if object
                                (m/store-object! context #:m{:scope-id scope-id :local-id local-id :object object}
                                                 (fn [err data]
                                                   (if err
                                                     (callback err)
                                                     (callback nil data))))
                                (callback)))))
            (callback)))
        (callback)))))

;---------------
; Object api
;---------------

(defn resolve-ref [context {:m/keys [ref] :as in} callback]
  (if (keyword? ref)
    (m/get-object context #:m{:object-id ref}
                  (fn [err data]
                    (if err
                      (callback err)
                      (callback nil (or data ref)))))
    (callback nil ref)))

(defn get [context {:m/keys [obj prop] :as in} callback]
  (m/resolve-ref context #:m{:ref obj}
                 (fn [err object]
                   (if err
                     (callback err)
                     (m/resolve-ref context #:m{:ref (clojure.core/get obj prop)} callback)))))

(defn get-in [context {:m/keys [obj props] :as in} callback]
  (get #:m{:obj obj :prop (first props)} context
       (fn [err object]
         (if err
           (callback err)
           (if (> (count props) 1)
             (get-in #:m{:obj obj :props (rest props)} context callback)
             (callback nil obj))))))

;---------------
; Functions
;---------------

(defn build-function-call [context {:m/keys [f args default-args]} callback]
  ; get-object f -> f
  ; if all required args provided
  #:m{:type      :m/FunctionCall
      :function  :test/make-twice
      :arguments [#:m{:type           :m/FunctionArgumentValue
                      :argument-name  :test/in
                      :argument-value 1}]}
  ; else
  ; m/collect-objects missing-arguments default-args
  )

#_(defn call-function [context {:m/keys [f args] :as in} callback]
    (resolve-ref context #:m{:ref f}
                 (fn [err f]
                   (if err
                     (callback err)
                     (if (= :m/Function (:m/type f))
                       (get-fn-impl context #:m{:fn-id (:m/id f)}
                                    (fn [err fn-impl]
                                      (if err
                                        (callback err)
                                        (if fn-impl
                                          (try
                                            (m/send-function-start-statistics context nil (fn []))
                                            (fn-impl args callback)
                                            (catch Throwable t
                                              (callback #:m{:type    :m/Exception
                                                            :message (str "An exception occurred executing function " (:m/id f) ": " (.getMessage t))}))
                                            (finally
                                              (m/send-function-end-statistics context nil (fn []))))
                                          (callback #:m{:type    :m/Exception
                                                        :message (str "Could not find implementation of function " (:m/id f))})))))
                       (callback #:m{:type    :m/Exception
                                     :message (str "Could not find definition of function " f)}))))))

;---------------
; Eval
;---------------

(defn- arguments-map [context {:m/keys [args] :as in} callback]
  (if (empty? args)
    (callback nil args)
    (let [arg (first args)
          args-rest (rest args)]
      (assert-type arg :m/FunctionArgumentValue)
      (get context #:m{:obj arg :prop :m/argument-name}
           (fn [err arg-name]
             (if err
               (callback err)
               (get context #:m{:obj arg :prop :m/argument-value}
                    (fn [err arg-value]
                      (if err
                        (callback err)
                        (arguments-map context #:m{:args args-rest}
                                       (fn [err args-rest]
                                         (if err
                                           (callback err)
                                           (callback nil (merge
                                                           {arg-name arg-value}
                                                           args-rest))))))))))))))

(defn- eval-function-call [context {:m/keys [fc] :as in} callback]
  (get context #:m{:obj fc :prop :m/function}
       (fn [err f]
         (if err
           (callback err)
           (get context #:m{:obj fc :prop :m/arguments}
                (fn [err args]
                  (if err
                    (callback err)
                    (arguments-map context #:m{:args args}
                                   (fn [err args-map]
                                     (if err
                                       (callback err)
                                       (m/call-m-async-fn (:m/id f) context
                                                          (assoc args-map :m/target-node-labels #{:m/external-fns-runner})
                                                          callback)
                                       #_(m/call-function context
                                                          #:m{:f f :args args-map}
                                                          callback)))))))))))

(defn- eval-item [context {:m/keys [form resolve-refs] :as in} callback]
  (cond
    (m/instance? context #:m{:class :m/FunctionCall :obj form})
    (eval-function-call context #:m{:fc form} callback)

    :else
    (if resolve-refs
      (m/resolve-ref context #:m{:ref form}
                     (fn [err form-resolved]
                       (if err
                         (callback err)
                         (eval-item context #:m{:form form-resolved} callback))))
      (callback nil form))))

(defn eval [context {:m/keys [form resolve-refs] :as in} callback]
  (w/postwalk-async (fn [o cbk]
                      (eval-item context #:m{:form o :resolve-refs resolve-refs} cbk))
                    form callback))

;---------------
; Init
;---------------
(defn new-node [context {:m/keys [node-id cost labels config default-target-node-filters default-scope fns] :as in}]
  #:m{:id                          node-id
      :cost                        cost
      :labels                      (apply conj #{:m/memory-node
                                                 :m/util-api :m/scope-api :m/type-api :m/store-api
                                                 :m/object-api :m/fn-run-api :m/eval-api :m/external-fns-runner}
                                          labels)
      :config                      config
      :initial-state               #:m{; The default scope to look for variables in the node
                                       :default-scope default-scope
                                       ; Scope ids can be put in to a hierarchy: the global Clojure hierarchy will be used.
                                       :scope-ids     #{}
                                       ; The M object store is implemented as a map
                                       ; mapping each scope to an atom containing a map of objects mapped by id.
                                       :object-store  {}
                                       ; The schema is stored in a map of the form:
                                       ; {:types {}
                                       ;  ...}
                                       :schema        {}}
      :api-version                 1
      :api                         #:m{:fn-specs [] ; fn specs
                                       :fns-by-spec ; fn implementations mapped by spec id
                                                 #:m{:get-default-scope              get-default-scope
                                                     :set-default-scope!             set-default-scope!
                                                     :init-scope!                    init-scope!
                                                     :assert-scope                   assert-scope
                                                     :remove-scope!                  remove-scope!

                                                     :get-local-str-id               get-local-str-id
                                                     :get-scope-str-id               get-scope-str-id
                                                     :to-keyword                     to-keyword
                                                     :get-local-id                   get-local-id
                                                     :get-scope-id                   get-scope-id
                                                     :instance?                      instance?

                                                     :store-object!                  store-object!
                                                     :get-object                     get-object
                                                     :resolve-ref                    resolve-ref
                                                     :assert-type                    assert-type

                                                     :get                            get
                                                     :get-in                         get-in

                                                     ; :call-function                  call-function
                                                     :send-function-start-statistics send-function-start-statistics
                                                     :send-function-end-statistics   send-function-end-statistics

                                                     :eval                           eval}
                                       :fns ; fn implementation with their spec (or
                                       ; spec-id) and implementation, in one shot
                                                 (concat fns
                                                         [#_#:m{:id      :mm/fn-impl-id
                                                                :spec-id :m/spec-id
                                                                ; or
                                                                :spec    {}
                                                                :impl    (fn [...])}])}
      :default-target-node-filters (merge m/default-target-node-filters
                                          default-target-node-filters)})
