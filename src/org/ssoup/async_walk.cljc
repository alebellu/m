(ns ^{:author "Alessandro Bellucci",
      :doc    "Async version of clojure.walk
      This file defines a generic tree walker for Clojure data
structures.  It takes any data structure (list, vector, map, set,
seq), calls an asynchronous function on every element, and uses the return value
of the function in place of the original."}
org.ssoup.async-walk
  #_(:require
      ? (:clj [eu.ssoup.m.async :refer [go <? <?all]]))
  ;#?(:cljs (require-macros [eu.ssoup.m.async-cljs :refer [go <? <?all]]))
  )

(declare walk-async #_[inner outer cbk form])

(defn map-async [f s callback]
  (let [rets (atom (vec (repeat (count s) nil)))
        total-count (atom 0)]
    (doall
      (map-indexed
        (fn [idx el]
          (f el (fn [err ret]
                  (swap! rets #(assoc % idx (if err {:error err} ret)))
                  (when (= (swap! total-count inc) (count s)) ; when all elements resolved
                    (callback nil @rets)))))
        s))))

(defn walk-async [inner outer form callback]
  "Traverses form, an arbitrary data structure. inner and outer are asynchronous
   functions accepting a callback as the first argument.
   Applies inner to each element of form, building up a
   data structure of the same type, then applies outer to the result.
   Recognizes all Clojure data structures. Consumes seqs as with doall."
  (cond
    (list? form)
    (map-async inner form (fn [err res] (outer (apply list res) callback)))

    (map-entry? form)
    (map-async inner form (fn [err res] (outer (vec res) callback)))

    (seq? form)
    (map-async inner form (fn [err res] (outer (doall res) callback)))

    (record? form)
    (map-async inner form (fn [err res] (outer (reduce (fn [r x] (conj r x)) form res) callback)))

    (coll? form)
    (map-async inner form (fn [err res] (outer (into (empty form) res) callback)))

    :else (outer form callback)))

(defn postwalk-async
  "Performs a depth-first, post-order traversal of form.
  f is an asynchronous function accepting a callback as the first argument.
  Calls f on each sub-form, uses f's return value in place of the original.
  Recognizes all Clojure data structures. Consumes seqs as with doall."
  [f form callback]
  (walk-async (partial postwalk-async f) f form callback))

(defn prewalk-async
  "Like postwalk, but does pre-order traversal."
  [f form callback]
  (f form (fn [err ret] (walk-async (partial prewalk-async f) identity ret callback))))

#_(defn walk-core-async [inner outer form]
    (go
      (println form)
      (cond
        (list? form) (<? (outer (<?all (apply list (map inner form)))))
        (seq? form) (<? (outer (<?all (doall (map inner form)))))
        (record? form) (<? (outer (<?all (reduce (fn [r x] (conj r (inner x))) form form))))
        (coll? form) (<? (outer (into (empty form) (<?all (map inner form)))))
        :else (<? (outer form)))))

#_(defn postwalk-core-async
    "Performs a depth-first, post-order traversal of form.  Calls f on
    each sub-form, which must return a channel, and wait for a value from the channel.
    Uses f's return value from the channel in place of the original.
    Recognizes all Clojure data structures. Consumes seqs as with doall.
    Returns a channel which will contain the evaluated form."
    [f form]
    (walk-async (partial postwalk-async f) f form))

#_(defn prewalk-async
    "Like postwalk-async, but does pre-order traversal."
    [f form]
    (go
      (<? (walk-async (partial prewalk-async f) (fn [x] (go x)) (<? (f form))))))

