# m

The M library ( M stands for Meta, or Model , or Memory store )
is a CLJC implementation of a weakly typed object memory store with the ability
to connect to upstream M stores and to accept connections from downstream M stores.

M can integrate with an external user management system, to support authorization
mechanisms to the objects in the store (User authentication is not part of the M library).
It also integrates the management of per-type, per-tag and per-object user preferences and audit logs.

M also supports internationalization, by providing a special i18n string type and
the integration with the user management system and with an external message bundle.

Finally, M can connect to persistent stores (for example: file system, database, browser local storage)
to store objects and audit logs.
M abstracts from the specific persistence method, which can be
specified via plugins (M-file-store, M-db-store, M-browser-store, ...).

The type system is described in the excel attached.
The connection details to other M stores (network protocol,etc.) are abstracted in M, and
should be plugged in by the consumer of the library.

More advanced features:
- publish/subscribe on a per-object/per-type/per-tag basis,at both the local store level and
  at inter-store level (for example using websockets to connect the browser store to the server store).
- objects dependency graph : the objective is to get a reactive system with excel-like behavior

M api:
- getObjectByTypeAndId(objectId, typeId):
    Returns the object of the given type identified by the given id.
    It might be fetched from upstream engines, depending on the type fetch policy.
	IMPORTANT: only top level objects will be found,objects nested inside property values cannot be
	retrieved separately. If that is what is needed, then they should be given a name and
	referenced from the property, instead of embedding it.
- getObjectsByTypeAndTag(objectId, tags):
    Returns a list with all the objects of a specified type, tagged with some set of tags (pagination will be needed here)
- setObjectById(objectId, newValue):
    The object identified by the given id will be modified.
