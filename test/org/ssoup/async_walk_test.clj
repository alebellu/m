(ns org.ssoup.async-walk-test
  (:require [clojure.test :refer :all]
            [org.ssoup.async-walk :as wa]))

(defrecord TestRecord [a b c])

(deftest make-twice-test
  (let [g (fn [in]
            (let [ret (atom nil)]
              (wa/postwalk-async (fn [n cbk] (if (number? n) (cbk nil (* 2 n)) (cbk nil n)))
                                 in
                                 (fn [err out]
                                   (reset! ret out)))
              @ret))]
    (is (= '(2 4 10) (g '(1 2 5))))
    (is (= {:a 2 :b 4 :c 10} (g {:a 1 :b 2 :c 5})))
    (is (= (seq [2 4 10]) (g (seq [1 2 5]))))
    (is (= (map->TestRecord {:a 2 :b 4 :c 10}) (g (map->TestRecord {:a 1 :b 2 :c 5}))))
    (is (= [2 4 10] (g [1 2 5])))
    (is (= 4 (g 2)))
    (is (= :d (g :d)))))
