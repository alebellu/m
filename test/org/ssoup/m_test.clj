(ns org.ssoup.m-test
  (:require [clojure.test :refer :all]
            [org.ssoup.m :as m]
            [org.ssoup.m-memory :as mm]))

(def default-scope :s/application)

(def context m/default-ctx)

; Helpers

(defn store-object-sync!
  [in]
  (m/store-object! context in
                   (fn [err data]
                     (when err
                       (println "Error storing object: " err)))))

(defn get-object-sync [in]
  (let [ret (atom nil)]
    (m/get-object context in
                  (fn [err data]
                    (when-not err
                      (reset! ret data))))
    @ret))

(defn eval-sync
  ([form]
   (eval-sync form false))
  ([form resolve-refs]
   (let [ret (atom nil)]
     (m/eval context #:m{:form form :resolve-refs resolve-refs}
             (fn [err data]
               (when-not err
                 (reset! ret data))))
     @ret)))

(defn get-sync [obj prop]
  (let [ret (atom nil)]
    (m/get context #:m{:obj obj :prop prop}
           (fn [err out]
             (when-not err
               (reset! ret out))))
    @ret))

(defn resolve-ref-sync [ref]
  (let [ret (atom nil)]
    (m/resolve-ref context #:m{:ref ref}
                   (fn [err out]
                     (when-not err
                       (reset! ret out))))
    @ret))

; Fixtures

(defn make-twice [context {in :test/in :as args} callback]
  (assert (number? in) (str in "is not a number"))
  (callback nil (* 2 in)))

(defn init-nodes [f]
  (let [memory-node (mm/new-node context #:m{:node-id :t/test-memory-node
                                             :cost    1
                                             :fns {:test/make-twice make-twice}})]
    (m/init #:m{:nodes        [memory-node]
                :group-labels #{:t/test-node}}
            (fn [err nodes]
              (f)))))

(defn init-scope [f]
  (m/init-scope! context #:m{:scope-id       default-scope
                             :set-as-default true})
  (f))

(defn init-data [f]
  (store-object-sync! #:m{:local-id :test/a
                          :object   1})
  (store-object-sync! #:m{:object #:m{:id                  :test/make-twice
                                      :type                :m/Function
                                      :arguments           [#:m{:type          :m/FunctionArgument
                                                                :argument-name :test/in
                                                                :argument-type :double}]
                                      :returnType          :double
                                      :programmingLanguage :clj
                                      :runsOn              :browser
                                      :pure                true}})
  (f))

(use-fixtures :once init-nodes init-scope init-data)

(deftest id-handling-tests
  (is (= ":ns1/a" (m/get-local-str-id context #:m{:object-str-id ":ns1/a@:ns2/s1"})))
  (is (= ":ns1/a" (m/get-local-str-id context #:m{:object-str-id ":ns1/a"})))
  (is (= :ns1/a (m/get-local-id context #:m{:object-id ":ns1/a@:ns2/s1"})))
  (is (= :ns1/a (m/get-local-id context #:m{:object-id ":ns1/a"})))
  (is (= :ns1/a (m/get-local-id context #:m{:object-id :ns1/a})))

  (is (= ":ns2/s1" (m/get-scope-str-id context #:m{:object-str-id ":ns1/a@:ns2/s1"})))
  (is (nil? (m/get-scope-str-id context #:m{:object-str-id ":ns1/a"})))
  (is (= :ns2/s1 (m/get-scope-id context #:m{:object-id ":ns1/a@:ns2/s1"})))
  (is (nil? (m/get-scope-id context #:m{:object-id ":ns1/a"})))
  (is (nil? (m/get-scope-id context #:m{:object-id :ns1/a})))
  (is (= default-scope (m/get-scope-id context #:m{:object-id ":ns1/a" :return-current-if-not-found true})))
  (is (= default-scope (m/get-scope-id context #:m{:object-id :ns1/a :return-current-if-not-found true}))))

; Store and get

(deftest store-and-get-tests
  (let [g1 (fn [object-id object prop]
             (store-object-sync! #:m{:local-id object-id :object object})
             (get-sync (get-object-sync #:m{:object-id object-id}) prop))
        g (fn [object prop]
            (let [object-id (:m/id object)]
              (g1 object-id object prop)))
        g2 (fn [scope-id object-id prop]
             (store-object-sync! #:m{:scope-id scope-id :local-id object-id})
             (get (get-object-sync #:m{:scope-id scope-id :object-id object-id}) prop))]
    (is (= 1 (g {:m/id :ns1/a :ns1/p 1} :ns1/p)))))

; Refs

(deftest refs-tests
  (is (= 1 (resolve-ref-sync :test/a)))
  (is (= 1 (get-sync {:a :test/a} :a))))

; Eval

(deftest eval-test
  (is (= 1 (eval-sync 1)))
  (is (= [1 2] (eval-sync [1 2])))
  (is (= {:a 1 :b 2} (eval-sync {:a 1 :b 2})))
  (is (= 1 (eval-sync :test/a true)))
  (is (= {:a 1} (eval-sync {:a :test/a} true)))
  (is (= 2 (eval-sync #:m{:type      :m/FunctionCall
                          :function  :test/make-twice
                          :arguments [#:m{:type           :m/FunctionArgumentValue
                                          :argument-name  :test/in
                                          :argument-value 1}]}))))
