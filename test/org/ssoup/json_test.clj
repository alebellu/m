(ns org.ssoup.json-test
  (:require [clojure.test :refer :all]
            [org.ssoup.m :as m]
            [org.ssoup.json :as json]))

(def default-scope :s/application)

(def context m/default-ctx)

; Helpers

; Fixtures

; Tests
(deftest deserialization-tests
  (is (= 1 (json/parse-str "1")))
  (is (= "a" (json/parse-str "\"a\"")))
  (is (= [1] (json/parse-str "[1]")))
  (is (= ["a"] (json/parse-str "[\"a\"]")))
  (is (= :a (json/parse-str "\":a\"")))
  (is (= ":a" (json/parse-str "\"#str#:a\"")))
  (is (= :a/b (json/parse-str "\":a/b\"")))
  (is (= {:a 1 :b "test"} (json/parse-str "{\":a\":1,\":b\":\"test\"}")))
  (is (= #{1 2 3 4} (json/parse-str "{\"#set#\":[1,2,3,4]}")))
  (is (= (list 1 2 3 4) (json/parse-str "{\"#list#\":[1,2,3,4]}")))
  (is (= [1 2 3 4] (json/parse-str "{\"#vector#\":[1,2,3,4]}"))))

(deftest serialization-tests
  (is (= "1" (json/write-str 1)))
  (is (= "\"a\"" (json/write-str "a")))
  (is (= "[1]" (json/write-str [1])))
  (is (= "[\"a\"]" (json/write-str ["a"])))
  (is (= "\":a\"" (json/write-str :a)))
  (is (= "\"#str#:a\"" (json/write-str ":a")))
  (is (= "\":a/b\"" (json/write-str :a/b)))
  (is (= "{\":a\":1,\":b\":\"test\"}" (json/write-str {:a 1 :b "test"})))
  (is (and (clojure.string/starts-with? (json/write-str #{1 2 3 4}) "{\"#set#\":[")
           (= #{1 2 3 4} (json/parse-str (json/write-str #{1 2 3 4})))))
  (is (= "{\"#list#\":[1,2,3,4]}" (json/write-str (list 1 2 3 4)))))
