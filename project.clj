(defproject m "0.1.0-SNAPSHOT"
  :description "m"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/core.async "0.4.474"]
                 [org.clojure/tools.reader "1.2.1"]
                 [clj-time "0.12.0"]

                 ; xml parsing
                 [org.clojure/data.xml "0.2.0-alpha5"]
                 [org.clojure/data.zip "0.1.2"]

                 ; Logging
                 [com.taoensso/timbre "4.3.1"]
                 ; The following deps are needed by Pedestal
                 [ch.qos.logback/logback-classic "1.1.6" :exclusions [org.slf4j/slf4j-api]]
                 [org.slf4j/jul-to-slf4j "1.7.18"]
                 [org.slf4j/jcl-over-slf4j "1.7.18"]
                 [org.slf4j/log4j-over-slf4j "1.7.18"]
                 [org.clojure/tools.logging "0.3.1"]])

